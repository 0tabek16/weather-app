import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import moment from "moment";
import './App.css';
// import data from "./weather.json";

import BaseIcon from "./components/Base-Icon";
import OneDay from "./components/OneDay";
import Axios from 'axios';

class App extends Component {
  constructor() {
    super();
    this.state = {
      data: {},
      isMetric: true,
      city: "Tashkent",
      owmCityName: '',
      error: 'chu chu'
    };
  }
  componentDidMount() {
    this.fetchDataFromAPI()
  }
  fetchDataFromAPI = async e => {
    try {

      await this.setState({
        error: '' // Clear errors, if there is any
      })

      let url = `https://api.openweathermap.org/data/2.5/forecast?appid=5c22422f5c35dc74763c2b6f4074f781&q=${this.state.city}`
      url += ('&units=' + (this.state.isMetric ? 'metric' : 'imperial'))

      const res = await Axios.get(url)
      await this.parseData(res.data.list)
      await this.setState({
        owmCityName: res.data.city.name
      })

    } catch (err) {
      await this.setState({
        error: err.response.data.message
      })
    }
  }
  parseData(list) {
    const data = {}

    list.forEach((item) => {
      const day = moment(item.dt * 1000).format("dddd")
      if (!data[day]) data[day] = []
      data[day].push(item)
    })

    this.setState({
      data: data
    })
  }
  renderList() {
    const weatherData = this.state.data;
    const list = Object.keys(weatherData).map(item => {
      const currentData = weatherData[item][0]

      const temp = currentData.main.temp.toString().split('.')[0]; // Get whole number
      const desc = currentData.weather[0].description; // Get description
      const key = currentData.dt; // Using DT as a unique id
      const iconName = currentData.weather[0].main; // Get Icon name

      return (
        <li key={key} title={desc}>
          <Link to={"/" + item}>
            <span className="App-temp">
              {temp} &deg;{this.state.isMetric ? 'C' : 'F'}
            </span>
            <BaseIcon iconName={iconName} />
            <span className="App-day-title">
              {item}
            </span>
          </Link>
        </li>
      )
    })

    const Routes = Object.keys(weatherData).map(day => {
      return (
        <Route key={day} path={"/" + day} exact component={
          () => <OneDay day={day} data={weatherData[day]} />
        } />
      )
    })

    return (
      <div className="container">
        <ul className="App-list">
          {list}
        </ul>
        {Routes}
      </div>
    )
  }
  changeUnit = async e => {
    await this.setState({
      isMetric: !this.state.isMetric
    })
    await this.fetchDataFromAPI()
  }
  onInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  handleSubmit = e => {
    e.preventDefault()
    console.log(this.state.city.length);

    if (this.state.city.length < 2) return this.setState({
      error: 'Invalid input'
    })

    return this.fetchDataFromAPI()
  }
  render() {
    return (
      <div className="App">
        <h1 className="App-title">
          Weather App
          <span id="unitChanger" className="App-change-unit" onClick={this.changeUnit}> {this.state.isMetric ? "\u00B0C" : "\u00B0F"}</span>
        </h1>
        <form onSubmit={this.handleSubmit}>
          <input className="App-input" onChange={this.onInputChange} value={this.state.city} name="city" type="text" placeholder="City" />
          <p className="App-error-msg">{this.state.error}</p>
          <input type="submit" hidden />
        </form>
        <Router>
          {this.renderList()}
        </Router>
        <h2 className="App-secondary-title">{this.state.owmCityName}</h2>
      </div>
    );
  }
}

export default App;
