import ContextProvider, { MainContext } from './Context'

export default ContextProvider
export { MainContext }
