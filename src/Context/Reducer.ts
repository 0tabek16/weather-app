import { ACTIONS } from '../constants'
import { List, City } from '../interfaces'

export const InitialState = {
  theme: 'light',
  city: 'Tashkent',
  weather: {},
  unit: true
}

export interface InitialStateType {
  theme: string
  city: string
  weather: {
    list?: {
      [key: string]: List[]
    }
    city?: City
  }
  unit: boolean
}

export function reducer(
  state: InitialStateType,
  { type, payload }: ActionType
): InitialStateType {
  switch (type) {
    case ACTIONS.SET_THEME:
      return { ...state, theme: payload }
    case ACTIONS.SET_CITY:
      return { ...state, city: payload }
    case ACTIONS.SET_WEATHER:
      return { ...state, weather: payload }
    case ACTIONS.TOGGLE_UNIT:
      return { ...state, unit: !state.unit }
    default:
      throw new Error('Unknown action')
  }
}

export interface ActionType {
  type: string
  payload: any
}

export interface ContextType {
  state: InitialStateType
  dispatch: (type: string, payload?: any) => void
}
