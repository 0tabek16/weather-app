import React, { createContext, useReducer, useEffect } from 'react'
import { InitialState, reducer, ContextType } from './Reducer'
import axios from 'axios'
import { OWM_API_URL, ACTIONS } from '../constants'
import { List } from '../interfaces'
import moment from 'moment'

const MainContext = createContext({} as ContextType)

const ContextProvider: React.FC = ({ children }) => {
  const [state, reducerDispatch] = useReducer(reducer, InitialState)
  const dispatch = (type: string, payload: any) => {
    return reducerDispatch({
      type,
      payload
    })
  }
  const isDarkThemeEnabled = state.theme === 'dark' ? 'dark' : ''
  const mainClassNames = ['App', isDarkThemeEnabled].join(' ')
  const { city, unit } = state

  useEffect(() => {
    fetchWeatherData()
  }, [city, unit])

  async function fetchWeatherData() {
    const res = await axios.get(
      `${OWM_API_URL}&q=${city}&units=${unit ? 'metric' : 'imperial'}`
    )

    let list: any = {}

    res.data.list.forEach((item: List) => {
      const day = moment(item.dt_txt)
        .format('dddd')
        .toLowerCase()
      if (!list[day]) list[day] = []
      list[day].push(item)
    })

    dispatch(ACTIONS.SET_WEATHER, { list, city: res.data.city })
  }

  return (
    <MainContext.Provider value={{ dispatch, state }}>
      <div className={mainClassNames}>{children}</div>
    </MainContext.Provider>
  )
}

export default ContextProvider

export { MainContext }
