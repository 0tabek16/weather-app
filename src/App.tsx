import React, { FormEvent, useContext, useState } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  Redirect
} from 'react-router-dom'
import { MainContext } from './Context'
import { ACTIONS } from './constants'
import { Home, UnitSign } from './components/Home'
import { OneDay } from './components/OneDay'
import { Cloud } from './components/Cloud'

const App: React.FC = () => {
  const { state, dispatch } = useContext(MainContext)
  const [city, setCity] = useState(state.city)

  function submitHandler(evt: FormEvent<HTMLFormElement>) {
    evt.preventDefault()
    dispatch(ACTIONS.SET_CITY, city)
  }

  return (
    <Router>
      <h1 className="AppTitle">
        <Link to="/">Weather</Link>
        <Link
          className="AppTitleUnitToggler"
          to=""
          role="button"
          onClick={() => dispatch(ACTIONS.TOGGLE_UNIT)}>
          <UnitSign />
        </Link>
      </h1>
      <form onSubmit={submitHandler}>
        <input
          className="AppInput BlurBackground"
          type="text"
          name="city"
          value={city}
          onChange={evt => setCity(evt.target.value)}
        />
        <input type="submit" hidden />
      </form>
      <Switch>
        <Route exact path="/" component={Home} />
        {state.weather.list &&
          Object.keys(state.weather.list).map(day => {
            return (
              <Route
                exact
                path={`/${day}`}
                component={OneDay}
                key={`DayRoute_${day}`}
              />
            )
          })}
        <Route exact path="*" component={() => <Redirect to="/" />} />
      </Switch>
      <h3 className="AppBottomTitle">
        <span>
          {state.weather.city?.name}, {state.weather.city?.country}
        </span>
      </h3>
      <Cloud seed={8517} />
    </Router>
  )
}

export default App
