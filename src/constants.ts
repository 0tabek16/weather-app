export const APP_TITLE = 'Weather'

export const ACTIONS = {
  SET_THEME: 'setTheme',
  SET_WEATHER: 'setWeather',
  SET_CITY: 'setCity',
  TOGGLE_UNIT: 'toggleUnit'
}

export const WEATHER_ICONS = {
  RAIN: 'Rain',
  SNOW: 'Snow',
  CLEAR: 'Clear',
  CLOUDS: 'Clouds'
}

const { REACT_APP_OWM_API_KEY } = process.env

export const OWM_API_URL = `https://api.openweathermap.org/data/2.5/forecast?appid=${REACT_APP_OWM_API_KEY}`
