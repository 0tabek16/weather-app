import React, { useContext } from 'react'
import { MainContext } from '../../Context'

interface UnitSignPropType {}

const UnitSign: React.FC<UnitSignPropType> = () => {
  const {
    state: { unit }
  } = useContext(MainContext)

  return <span>&deg;{unit ? 'C' : 'F'}</span>
}

export default UnitSign
