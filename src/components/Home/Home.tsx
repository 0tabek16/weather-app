import React, { useContext } from 'react'
import { MainContext } from '../../Context'
import { Link } from 'react-router-dom'
import { ListItem } from '../ListItem'

interface HomePropType {}

const Home: React.FC<HomePropType> = () => {
  const { state } = useContext(MainContext)
  return (
    <div className="AppList">
      {state.weather.list &&
        Object.keys(state.weather.list).map((day, i) => {
          // @ts-ignore
          const currentData = state.weather.list[day][0]
          const temp = Math.floor(currentData.main.temp)
          const desc = currentData.weather[0].description // Get description
          const iconName = currentData.weather[0].main // Get Icon name
          return (
            <Link to={`/${day}`} key={`Day_${i}`} title={desc}>
              <ListItem time={day} iconName={iconName} temp={temp} />
            </Link>
          )
        })}
    </div>
  )
}

export default Home
