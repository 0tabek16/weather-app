import React from 'react'
import { Icon } from '../Icons'
import { UnitSign } from '../Home'

interface ListItemPropType {
  time: string
  iconName: string
  temp: number
}

const ListItem: React.FC<ListItemPropType> = ({ iconName, time, temp }) => {
  return (
    <div className="AppListItem BlurBackground">
      <div className="AppListItem-Day">{time}</div>
      <Icon icon={iconName} />
      <div className="AppListItem-Degree">
        {temp} <UnitSign />
      </div>
    </div>
  )
}

export default ListItem
