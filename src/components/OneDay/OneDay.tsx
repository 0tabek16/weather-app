import React, { useContext } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { MainContext } from '../../Context'
import { ListItem } from '../ListItem'
import moment from 'moment'

interface OneDayPropType extends RouteComponentProps {}

const OneDay: React.FC<OneDayPropType> = ({ location }) => {
  const { state } = useContext(MainContext)
  const day = location.pathname.substring(1)
  return (
    <div className="AppList">
      {state.weather.list &&
        state.weather.list[day].map((time, i) => {
          const displayTime = moment(time.dt_txt).format('LT')
          const temp = Math.floor(time.main.temp)
          const iconName = time.weather[0].main // Get Icon name
          return (
            <ListItem
              time={displayTime}
              iconName={iconName}
              temp={temp}
              key={`Hourly_${i}`}
            />
          )
        })}
    </div>
  )
}

export default OneDay
